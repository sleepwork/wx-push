==Java版本，拉下代码直接能用小白也能懂得详细教程，代码在文章最后。==

> **代码包含，日记记录，定时推送，等功能。**
> 
> **数据库地址需要进行修改，修改yml文件，如果需要别的功能可以进行留言，我尽力进行添加，没有服务器的可以用手机，参考这一篇文章：[https://blog.csdn.net/weixin_45853881/article/details/126864672](https://blog.csdn.net/weixin_45853881/article/details/126864672)**



# 一.申请账号

> 点击登录，然后扫码登录就行

微信公众平台接口测试帐号申请：[https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login](https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login)

> 然后就会得到`appID`和`appsecret`

# 二.接口测试

## 1.调节连接接口

> **测试微信公众号消息推送需要外网，所以有两种方式，弄一个服务器，或者使用内网穿透**

> **我这里前面使用的内网穿透，后来换成了服务器，网站这里配置提交发送的请求是get请求**
> **在申请的公众号里面发消息，请求接口和这个是同一个，不过走的是post请求，这里需要注意一下**

**如果使用服务器需要注意，注意==>微信公众号接口必须以http://或https://开头，分别支持80端口和443端口。**
**如果使用的是内网穿透的话本地就不用注意了，就看看内网穿透上面的端口，大多数内网穿透都是443和80端口**

![在这里插入图片描述](https://img-blog.csdnimg.cn/993265f7e17e429ea44269759a634e11.png)

## 2.创建模板
![在这里插入图片描述](https://img-blog.csdnimg.cn/a7b1b79af0024b519650a13aaabb2c03.png)

```bash
//微信公众号发送消息
    public void pushTemplate(String openId){
        //1，配置
        WxMpInMemoryConfigStorage wxStorage = new WxMpInMemoryConfigStorage();
        wxStorage.setAppId(wxMpProperties.getAppId());
        wxStorage.setSecret(wxMpProperties.getSecret());
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxStorage);
        //2,推送消息
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder()
                .toUser(openId)//推送用户
                .templateId(pushTemplateId)//模板id
                .build();
        String text = "❤";
        templateMessage.addData(new WxMpTemplateData("text",text,"#FF0000"));
        try {
            wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
        } catch (Exception e) {
            log.error("推送失败：" + e.getMessage());
            e.printStackTrace();
        }
    }
```

> **注意，模板这里用的是`{{xxx.DATA}}`格式**
模板去`utils/WxUtils` 里面找，代码复制就可以进行使用
```bash
地址：{{cityName.DATA}} 

时间：{{weatherDate.DATA}} 

天气：{{weatherType.DATA}}	
```
**下面这样的推送，地点是写死的,可以自行修改，或者打开定位进行推送**
![在这里插入图片描述](https://img-blog.csdnimg.cn/d59eb03a6cd44d8d9b93b7284e038c90.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/22c656284a3148d5be6d12bdb1d8ddaa.png)


## 3.体验接口权限表

> **这里使用了公众号的定位权限，在下面开启。
因为代码需要，所有就开启一下获取当前定位，然后在获得该地的天气**

![在这里插入图片描述](https://img-blog.csdnimg.cn/8b518378d9d3464cb1598e21c74dd9fb.png)

## 4.修改配置
**修改application.yml文件**

> **把里面的修改成自己的进行使用，pushUser是list集合的形式，可以只留一个**
 
![在这里插入图片描述](https://img-blog.csdnimg.cn/60fef91d4e2442d98a3806808065988e.png)
下面的俩都是api接口官网，可以自己去申请，都有好多免费的api接口

[https://www.tianapi.com/](https://www.tianapi.com/)
[https://www.tianapi.com/](https://www.tianapi.com/)
> 这里使用的是高德地图的接口，使用的目的是`根据经纬获取地址`，经纬度获取用的就是微信公众号申请的定位
 
> 里面的key需要换成自己在高德申请的，当然也可以用过百度，腾讯等等都可以，这里用的就是根据经纬度获取当前地址。

高德开放平台：[https://lbs.amap.com/?ref=https://console.amap.com](https://lbs.amap.com/?ref=https://console.amap.com)
![在这里插入图片描述](https://img-blog.csdnimg.cn/f38525b126ab44b3b66ea311967be573.png)

还能设置回复很多消息，都是聪明人，自己搞吧，定时任务刚写完，下载的可以进行更新代码

```bash
//import org.springframework.scheduling.annotation.Scheduled;
// 定时任务==>测试
    //{秒数} {分钟} {小时} {日期} {月份} {星期} {年份(可为空)}
    @Scheduled(cron = "0 30 8 * * ?")
    public void test(){
        System.out.println("定时任务触发了");
        wxPushController.test();
    }
```

# 三.代码下载：
推送天气的接口改为输入关键字推送，加上在界面十分没有获取天气在界面就自动推送，可以进行取消，因为弄这个是测试没有想到很多人的情况，

如果使用人数多，可以使用公众号给的获取用户列表的这个接口，然后进行先获取在发送
![在这里插入图片描述](https://img-blog.csdnimg.cn/03aa51d1c6b247c6a87d73ba2f298954.png)
**直接下载更改参数就可以使用，更改`yml`文件
代码地址：[https://gitee.com/xu-kangyu/wx-push](https://gitee.com/xu-kangyu/wx-push)**

代码会不定时进行更新