-- 用户地址表
CREATE TABLE `user_area` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `open_id` varchar(50) DEFAULT NULL COMMENT 'openId，用户微信号',
  `name` varchar(20) DEFAULT NULL COMMENT '用户名',
  `longitude` varchar(20) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(20) DEFAULT NULL COMMENT '纬度',
  `precision1` varchar(20) DEFAULT NULL COMMENT '精度',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 用户日志表
CREATE TABLE `user_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `open_id` varchar(50) DEFAULT NULL COMMENT 'openId,用户',
  `type` int(11) DEFAULT 0 COMMENT '类型0平台发送1用户发送',
  `remark` varchar(20) DEFAULT NULL COMMENT '操作',
  `text` varchar(1024) DEFAULT NULL COMMENT '用户发送的消息，仅支持文本',
  `creat_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;