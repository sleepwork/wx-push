package com.ly.push.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ly.push.param.OtherProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class RainUtils {

    @Resource
    OtherProperties otherProperties;

    //土味情话api
    public String getRain() {
        String data = HttpUtils.doGet("http://api.tianapi.com/saylove/index?key="+otherProperties.getTxkey());
        JSONObject jsonObject = JSONObject.parseObject(data);
        String newsList = jsonObject.getString("newslist");
        String msg = jsonObject.getString("msg");
        if (msg.equals("success")){
            JSONArray jsonArray = JSONArray.parseArray(newsList);
            return jsonArray.getJSONObject(0).get("content").toString();
        }
        return null;
    }

    //彩虹屁pi
    public String getRain2() {
        String data = HttpUtils.doGet("http://api.tianapi.com/caihongpi/index?key="+otherProperties.getTxkey());
        JSONObject jsonObject = JSONObject.parseObject(data);
        String newsList = jsonObject.get("newslist").toString();
        String msg = jsonObject.get("msg").toString();
        if (msg.equals("success")){
            JSONArray jsonArray = JSONArray.parseArray(newsList);
            return jsonArray.getJSONObject(0).get("content").toString();
        }
        return null;
    }

    //英语一句话
    public Map<String,String> getRain3() {
        Map<String,String> map = new HashMap<>();
        String data = HttpUtils.doGet("https://apis.tianapi.com/ensentence/index?key="+otherProperties.getTxkey());
        JSONObject jsonObject = JSONObject.parseObject(data);
        if ("success".equals(jsonObject.getString("msg"))){
            String result = jsonObject.getString("result");
            JSONObject jsonObject1 = JSONObject.parseObject(result);
            String en = jsonObject1.getString("en");
            String zh = jsonObject1.getString("zh");
            map.put("en",en);
            map.put("zh",zh);
        }
        return map;
    }

    //每日心灵鸡汤Api
    public String getRain1() {
        String data = HttpUtils.doGet("https://apis.juhe.cn/fapig/soup/query?key="+otherProperties.getXlkey());
        JSONObject jsonObject = JSONObject.parseObject(data.toString());
        String result = jsonObject.get("result").toString();
        String reason = jsonObject.get("reason").toString();
        if (reason.equals("success")){
            JSONObject jsonObject1 = JSONObject.parseObject(result);
            return jsonObject1.get("text").toString();
        }
        return null;
    }

}
