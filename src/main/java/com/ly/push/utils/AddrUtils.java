package com.ly.push.utils;

import com.alibaba.fastjson.JSONObject;
import com.ly.push.param.OtherProperties;
import com.ly.push.param.WxMpProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

/**
 * ip，经纬度地址
 */
@Slf4j
@Component
public class AddrUtils {

    @Resource
    private WxMpProperties wxMpProperties;

    //获取本机内网ip
    public String getIp1(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getRemoteAddr();
            if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
                //根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ipAddress = inet.getHostAddress();
            }
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ipAddress != null && ipAddress.length() > 15) { //"***.***.***.***".length() = 15
            if (ipAddress.indexOf(",") > 0) {
                ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
            }
        }
        return ipAddress;
    }

    //获取本机外网ip
    public String getIP() {
        String result = HttpUtils.doGet("https://www.taobao.com/help/getip.php");
        String str = result.replace("ipCallback({ip:", "");
        String ipStr = str.replace("})", "");
        return ipStr.replace("\"", "");
    }

    //获取本机外网ip
    public static Map<String,String> getIPAddr() {
        String result = HttpUtils.doGet("https://pv.sohu.com/cityjson?ie=utf-8");
        String str = result.replace("var returnCitySN = {","{");
        String cip = str.substring(str.indexOf("{\"cip\": \"")+9,str.indexOf("\", \"cid\""));
        String cname = str.substring(str.indexOf("\"cname\": \"")+10,str.indexOf("\"};"));
        Map<String,String> map = new HashMap<>();
        map.put("ip",cip);
        map.put("cityName",cname);
        return map;
    }

    @Resource
    OtherProperties otherProperties;

    /**
     *根据经纬度获取省市区
     * 经度 log
     * 维度 lat
     */
    public Map<String,String> getAddr(String log, String lat){
        String key = otherProperties.getGdkey();
        String url = "https://restapi.amap.com/v3/geocode/regeo?location="+log+","+lat+"&extensions=base&batch=false&roadlevel=0&key="+key;
        String data = HttpUtils.doPost(url);
        //解析结果
        JSONObject jsonObject = JSONObject.parseObject(data);
        System.out.println(jsonObject.toJSONString());
        JSONObject jsonObject1 = jsonObject.getJSONObject("regeocode");
        String formatted_address = jsonObject1.getString("formatted_address");
        String addressComponent = jsonObject1.getString("addressComponent");
        JSONObject jsonObject2 = JSONObject.parseObject(addressComponent);
        String city = jsonObject2.getString("city");
        String province = jsonObject2.getString("province");
        Map<String, String> map = new HashMap<>();
        map.put("province", province);
        map.put("city", city);
        return map;
    }

}
