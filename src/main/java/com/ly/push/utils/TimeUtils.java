package com.ly.push.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间操作
 */
public class TimeUtils {

    //时间戳转换为时间===>String
    public static String longToDate(Long time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (time.toString().length()==10){
            time = time*1000;
        }
        return sdf.format(time);
    }

    //时间戳转换为时间===>Date
    public static Date longToDate1(Long time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (time.toString().length()==10){
            time = time*1000;
        }
        String time1 =  sdf.format(time);
        Date date = null;
        try {
            date = sdf.parse(time1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 之前的时间加上十分钟和现在时间比较
     * @param long1 之前的时间
     * @param long2 之后的时间
     *              这里计算十分张之后在进行发送
     */
    public static boolean compare(Long long1,Long long2){
        Calendar c1 = Calendar.getInstance();
        c1.setTime(longToDate1(long1));
        c1.add(Calendar.MINUTE, 10);
        Long time1 = c1.getTime().getTime();
        if (long2.toString().length()==10){
            long2 = long2*1000;
        }
        Long time2 = long2-time1;
        if (time2>0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 时间戳比较大小,如果ture就说明之后的时间大约之前的时间
     * @param long1 之前的时间
     * @param long2 之后的时间
     * @return
     */
    public static boolean compare1(Long long1,Long long2){
        if (long1.toString().length()==10){
            long1 = long1*1000;
        }
        if (long2.toString().length()==10){
            long2 = long2*1000;
        }
        Long time2 = long2-long1;
        if (time2>0){
            return true;
        }else {
            return false;
        }
    }

    //计算过去多少天，正计时
    public static Long calcDay(String day){
        //jdk8自带工具类：ChronoUtil
        Long days = 0L;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            //记录时间
            Calendar cDay = Calendar.getInstance();
            cDay.setTime(sdf.parse(day));
            //记录的时间
            LocalDate startDay = LocalDate.of(cDay.get(Calendar.YEAR), cDay.get(Calendar.MONTH)+1, cDay.get(Calendar.DATE));
            //当前时间
            Calendar cToday = Calendar.getInstance();
            //最后的时间，一般是当前时间
            LocalDate endDay = LocalDate.of(cToday.get(Calendar.YEAR), cToday.get(Calendar.MONTH)+1, cToday.get(Calendar.DATE));
            days = ChronoUnit.DAYS.between(startDay, endDay);
            System.out.println(days);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }

    //计算生日，倒计时
    public static int calcBirthday(String birthday){
        int days = 0;
        try {
            SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
            // 存今天
            Calendar cToday = Calendar.getInstance();
            // 存生日
            Calendar cBirth = Calendar.getInstance();
            // 设置生日
            cBirth.setTime(myFormatter.parse(birthday));
            // 修改为本年
            cBirth.set(Calendar.YEAR, cToday.get(Calendar.YEAR));
            if (cBirth.get(Calendar.DAY_OF_YEAR) < cToday.get(Calendar.DAY_OF_YEAR)) {
                // 生日已经过了，要算明年的了
                days = cToday.getActualMaximum(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
                days += cBirth.get(Calendar.DAY_OF_YEAR);
            } else {
                // 生日还没过
                days = cBirth.get(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }

    //Date转换为String
    public static String dateToString(Date time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(time);
    }

    //增加时间，例如当前时间增加 i 小时,指定时间，也可以写成当前时间
    public static String increaseTime(Date time,int i){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(time);
        c.add(Calendar.HOUR,i);
        return sdf.format(c.getTime());
    }
}
