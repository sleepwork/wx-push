package com.ly.push.config;

import com.ly.push.controller.QyWxPushController;
import com.ly.push.controller.WxPushController;
import com.ly.push.utils.TimeUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

//定时调用类
@Component
public class ScheduledConfig {

    @Resource
    WxPushController wxPushController;
    @Resource
    QyWxPushController qyWxPushController;

    // 定时任务==>测试
    //{秒数} {分钟} {小时} {日期} {月份} {星期} {年份(可为空)}d
//    @Scheduled(cron = "0 30 8 * * ?")
//    public void test(){
//        System.out.println("定时任务触发了");
//    }
//
//    @Scheduled(cron = "0/5 * * * * ?")//五秒触发一次
//    public void test1(){
//        System.out.println("每隔五秒触发一次："+TimeUtils.dateToString(new Date()));
//    }
//
//    @Scheduled(cron = "0 30 8 * * ?")
//    public void wxPushHost(){
//        wxPushController.wxPushHost();
//    }

    @Scheduled(cron = "0 30 5 * * ?")
    public void sendTextCard(){
        System.out.println("当前时间为："+TimeUtils.dateToString(new Date()));
        qyWxPushController.sendTextCard();
    }

    @Scheduled(cron = "0 30 22 * * ?")
    public void sendTextCard1(){
        System.out.println("当前时间为："+TimeUtils.dateToString(new Date()));
        qyWxPushController.sendTextCard1();
    }

}
