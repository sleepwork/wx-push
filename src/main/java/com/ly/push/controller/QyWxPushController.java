package com.ly.push.controller;

import com.ly.push.service.QyWxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/qy")
public class QyWxPushController {

    @Autowired
    private QyWxService qyWxService;

    @RequestMapping("/text")
    public void sendTextMsg(){
        qyWxService.sendTextMsg();
    }

    //status 0今天,1明天
    @RequestMapping("/textCard")
    public void sendTextCard(){
        qyWxService.sendTextCard(0);
    }

    @RequestMapping("/textCard1")
    public void sendTextCard1(){
        qyWxService.sendTextCard(1);
    }
}
