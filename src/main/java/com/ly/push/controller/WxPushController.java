package com.ly.push.controller;

import com.ly.push.param.wx.WxMsgBean;
import com.ly.push.service.WxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/wx")
public class WxPushController {

    @Autowired
    private WxService wxService;

    //测试
    @RequestMapping("/test")
    public void test(){
        wxService.test();
    }

    //验证
    @GetMapping("/wxCheck")
    public String wxCheckGet(@RequestParam(value = "signature") String signature,
                          @RequestParam(value = "timestamp") String timestamp,
                          @RequestParam(value = "nonce") String nonce,
                          @RequestParam(value = "echostr") String echostr){
        return wxService.wxCheckGet(signature, timestamp, nonce, echostr);
    }

    //接受用户消息，并且返回
    @PostMapping(value = "/wxCheck")
    public String wxCheckPost(@RequestBody WxMsgBean wxMsgBean,
                           @RequestParam(value = "signature") String signature,
                           @RequestParam(value = "timestamp") String timestamp,
                           @RequestParam(value = "nonce") String nonce){
        return wxService.wxCheckPost(wxMsgBean,signature, timestamp, nonce);
    }

    //发送消息
    @RequestMapping("/wxPush/hello")
    public void wxPushHello(){
        wxService.wxPushHello();
    }

    //发送日期
    @RequestMapping("/wxPush/date")
    public void wxPushDate(){
        wxService.wxPushDate();
    }

    //发送消息-天气
    @RequestMapping("/wxPush/host")
    public void wxPushHost(){
        wxService.wxPushHost();
    }

    /**************************************************企业微信**************************************************/
    @RequestMapping("/robot")
    public void qyWxRobot(){
        wxService.qyWxRobot();
    }

}
