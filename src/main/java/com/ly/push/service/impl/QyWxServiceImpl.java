package com.ly.push.service.impl;

import com.ly.push.service.QyWxService;
import com.ly.push.utils.QyWxUtils;
import com.ly.push.utils.WeatherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QyWxServiceImpl implements QyWxService {

    @Autowired
    QyWxUtils qyWxUtils;
    @Autowired
    WeatherUtils weatherUtils;


    @Override
    public void sendTextMsg() {
        String message = "你好";
        qyWxUtils.sendTextMsg("@all",message);
    }

    @Override
    public void sendTextCard(Integer index) {
        //标题
        String title = "今日份天气已到达";
        //设置卡片跳转链接
        String url = "https://user.qzone.qq.com/";
        qyWxUtils.sendTextCard("@all",title,url,index);
    }
}
