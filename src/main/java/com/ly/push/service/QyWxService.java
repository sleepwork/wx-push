package com.ly.push.service;

public interface QyWxService {

    /**
     * 推送文字消息
     */
    void sendTextMsg();

    /**
     * 推送文本卡片消息
     */
    void sendTextCard(Integer index);

}
