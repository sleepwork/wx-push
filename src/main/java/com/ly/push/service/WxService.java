package com.ly.push.service;

import com.ly.push.param.wx.WxMsgBean;

public interface WxService {

    //get
    String wxCheckGet(String signature, String timestamp, String nonce, String echostr);

    //post
    String wxCheckPost(WxMsgBean wxMsgBean, String signature, String timestamp, String nonce);

    void wxPushHello();

    void wxPushDate();

    void wxPushHost();

    void test();

    void qyWxRobot();
}
