package com.ly.push.param;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "mt")
public class MyTimeProperties {

    private String name1;
    private String birthday1;
    private String name2;
    private String birthday2;
    private String day1;
}
