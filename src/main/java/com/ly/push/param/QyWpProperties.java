package com.ly.push.param;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "qy")
public class QyWpProperties {

    private String agentId;
    private String secret;
    private String corpId;
}
