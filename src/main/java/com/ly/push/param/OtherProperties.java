package com.ly.push.param;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "other")
public class OtherProperties {

    private String gdkey;
    private String txkey;
    private String weatherkey;
    private String xlkey;
}
