package com.ly.push.param.wx;

import lombok.Data;

import java.util.List;

@Data
public class QyWxRobot {
    /**
     * 机器人id
     */
    private String robotId;
    /**
     * 机器人名字
     */
    private String robotName;
    /**
     * 当前机器人的webhook地址
     */
    private String webhookAddress;
    /**
     * 消息类型,当前自定义机器人支持文本（text）、markdown（markdown）、图片（image）、图文（news）四种消息类型。
     * 机器人的text/markdown类型消息支持在content中使用<@userid>扩展语法来@群成员
     */
    private String msgType;
    /**
     * 富文本框里面的内容
     */
    private String content;
    /**
     * 涉及发送的人员
     */
    private List<String> memberList;
    /**
     * 电话
     */
    private String mobileList;
    /**
     * 图片地址
     */
    private String imageUrl;
    /**
     * base64编码后的值
     */
    private String imageBase64Value;

    /**
     * 图片md5加密后的值
     */
    private String imageMd5Value;

    /**
     * 发送消息的标题
     */
    private String title;
    /**
     * 发送图文消息的描述信息
     */
    private String description;
    /**
     * 图片url地址集合
     */
    private List<String> imageUrlList;
    /**
     * 图片打开的地址
     */
    private String url;
    /**
     * 消息内容集合
     */
    private List<String> contentList;
    /**
     * 图片路径
     */
    private String savePath;
}
