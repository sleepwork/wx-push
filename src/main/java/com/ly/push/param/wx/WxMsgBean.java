package com.ly.push.param.wx;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * 微信公众号信息接入参数 bean
 */
@Data
//@XmlRootElement，类级别注解，主要属性为name，作用指定xml根节点的名称，因为微信发送的消息格式根节点为xml，所以设置name = "xml"
@XmlRootElement(name = "xml")
//@XmlAccessorType用于定义这个类中的哪一种类型需要映射到XMl中
//XmlAccessType.FIELD，映射这个类中的所有字段到XML
@XmlAccessorType(XmlAccessType.FIELD)
public class WxMsgBean implements Serializable {
    /**
     * 开发者微信号
     * 如果要按照驼峰命名法来命名属性，那么需要用@XmlElement注解来指定名称映射，例如：
     * @XmlElement("ToUserName")
     * private String toUserName;
     */
    private String ToUserName;

    /**
     * 发送方帐号（一个OpenID）
     */
    private String FromUserName;

    /**
     * 消息创建时间 （整型）
     */
    private Long CreateTime;

    /**
     * 消息类型: 文本为text，图片为image，语音为voice，视频为video，小视频为shortvideo，地理位置为location，链接为link，
     * 消息类型，event==>地址
     */
    private String MsgType;

    /**
     * 文本消息内容
     */
    private String Content;

    /**
     * 消息id，64位整型
     */
    private Long MsgId;

    /**
     * 消息的数据ID（消息如果来自文章时才有）
     */
    private Long MsgDataId;

    /**
     * 多图文时第几篇文章，从1开始（消息如果来自文章时才有）
     */
    private Long Idx;


    /**
     * 图片链接（由系统生成）
     */
    private String PicUrl;

    /**
     * 图片消息媒体id，可以调用获取临时素材接口拉取数据。
     * 语音消息媒体id，可以调用获取临时素材接口拉取数据。
     * 视频消息媒体id，可以调用获取临时素材接口拉取数据。
     */
    private String MediaId;

    /**
     * 语音格式，如amr，speex等
     */
    private String Format;

    /**
     * 语音识别结果，UTF8编码<br>
     * 需开通语音识别
     */
    private String Recognition;

    /**
     * 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    private String ThumbMediaId;

    /**
     * 地理位置消息：地理位置纬度
     */
    private String Location_X;

    /**
     * 地理位置消息：地理位置经度
     */
    private String Location_Y;

    /**
     * 地理位置消息：地图缩放大小
     */
    private String Scale;

    /**
     * 地理位置消息：地理位置信息
     */
    private String Label;

    /**
     * 链接消息：消息标题
     */
    private String Title;

    /**
     * 链接消息：消息描述
     */
    private String Description;

    /**Latitude
     *
     * 链接消息：消息链接
     */
    private String Url;

    /**
     * 事件类型 EVENT_TYPE_*
     * 事件类型，LOCATION
     */
    private String Event;

    /**
     * 扫描带参数二维码事件：用户未关注时，进行关注后的事件推送：事件KEY值，qrscene_为前缀，后面为二维码的参数值
     * 扫描带参数二维码事件：用户已关注时的事件推送：事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id
     * 自定义菜单事件：点击菜单拉取消息时的事件推送：事件KEY值，与自定义菜单接口中KEY值对应
     * 自定义菜单事件：点击菜单跳转链接时的事件推送：事件KEY值，设置的跳转URL
     */
    private String EventKey;

    /**
     * 扫描带参数二维码事件：二维码的ticket，可用来换取二维码图片
     */
    private String Ticket;

    /**
     * 上报地理位置事件：地理位置纬度
     */
    private String Latitude;

    /**
     * 上报地理位置事件：地理位置经度
     */
    private String Longitude;

    /**
     * 上报地理位置事件：地理位置精度
     */
    private String Precision;
}
