package com.ly.push.param;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
//连接yml

@Data
@Component
@ConfigurationProperties(prefix = "wx")
public class WxMpProperties {

    /**
     * 公众号appId
     */
    private String appId;

    /**
     * 公众号appSecret
     */
    private String secret;

    /**
     * 公众号token
     */
    private String token;

    /**
     * 公众号aesKey
     */
    private String aesKey;

    private List<String> pushUser;

    /**
     * 推送消息模板
     */
    private String pushTemplateId;

    private String pushTemplateId1;

    /**
     * 天气模板
     */
    private String weatherTemplateId;

    /**
     * 日期模板
     */
    private String birthdayTemplateId;

    private String webhook;
}
