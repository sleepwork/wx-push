package com.ly.push.param.weather;


import lombok.Data;

@Data
public class WeatherInfo1 {

    private String date;

    private String temperature;

    private String weather;

    private String direct;
}
