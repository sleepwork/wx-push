package com.ly.push.param.weather;

import lombok.Data;

/**
 * 天气
 */
@Data
public class WeatherInfo {
    private String date;
    private String high;
    private String fengli;
    private String low;
    private String fengxiang;
    private String type;
}
